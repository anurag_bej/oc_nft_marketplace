import { useEffect, useState, useRef } from 'react'
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom'

//Ethers.js library it will allow us to talk to ethereum nodes we have used it in test and deploy scripts
//to connect to local dev blockchain and we will use it for the same thing for our app
//Except ethers is going to connect to metamask and metamask is already connected to block-chain 
//So ethers will use metamask as ethereum providers , there are many types of providers the provider 
//Provided by metamask is called the web3 provider
import { ethers } from 'ethers';
import MyPurchases from './MyPurchases'
import Spinn from '../components/Spinner/Spinn';
import MarketplaceAbi from '../contractsData/Marketplace.json'
import MarketplaceAddress from '../contractsData/Marketplace-address.json'
import NFTAbi from '../contractsData/NFT.json'
import NFTAddress from '../contractsData/NFT-address.json'
import Navigation from '../components/Navbar/Navbar'
import Create from '../components/Create/Create';
import Home from '../components/Home/Home';
import Detail from './Detail';
import Login from '../components/Login/Login';
import Signup from '../components/Signup/Signup';
import ForgetPassword from '../components/Forget_password/ForgetPassword';
import MyListedItems from './MyListedItems'
import Footer from '../components/Footer/Footer';
import Loader from '../components/Loading/Loading'
import UserProfile from './User_profile/UserProfile';

const useStateWithCallback = (initialState) => {
  const [state, setState] = useState(initialState);
  const callbackRef = useRef(() => undefined);

  const setStateCB = (newState, callback) => {
    callbackRef.current = callback;
    setState(newState);
  };

  useEffect(() => {
    if (callbackRef.current) {
      callbackRef.current();
    }
  }, [state]);

  return [state, setStateCB];
};

function App() {
  const [loading, setLoading] = useState(true)
  const [account, setAccount] = useState(null);
  const [nft, setNFT] = useState({})
  const [isDataLoad, setIsDataLoad] = useState();
  const [marketplace, setMarketplace] = useStateWithCallback()
  const [tokID, setTOKID] = useState()
  const [itemID, setItemID] = useState()
  const [acToken,setacToken] = useState(null)
  const [bevent,setBevent] = useState()
  const [signer,setSigner] = useState()

  useEffect(() => {
    const provider = new ethers.providers.Web3Provider(window.ethereum)
    //Then we get the signer of the connected account from the provider
    const signer = provider.getSigner()
    loadContracts(signer)
  }, [])

  //Lets create a function which handles this connection with the metamask
  //Setup connection with blockchain
  const web3Handler = async () => {
    //Fetching the accounts listed in our metamask wallet
    //It will return array of accounts
    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    //from array of accounts the first account listed represent the account that is connected to the app
    //We are going to display address of that account on nav
    // if(acToken===null){<Login setacToken/>}else{
      setLoading(false)
    setAccount(accounts[0])
    //Got new provider from metamask by writing
    //after.providers specifying type of the provider web3provider
    //passing the window.ethereum object injected into the browser by the metamask
    const provider = new ethers.providers.Web3Provider(window.ethereum)
    // //Then we get the signer of the connected account from the provider
    const signer = provider.getSigner()

    
    // }
    
    console.log(account)
    return accounts[0]
  }
  const web3HandlerRemove = () => {
    setAccount(null)
    setLoading(true)
    const accounts = '';

  }
  //Next step load the contract from the blockchain for that we will create loadContracts function
  const loadContracts = async (signer) => {
    //We will fetch/get deployed copies of the contract
    const mk = new ethers.Contract(MarketplaceAddress.address, MarketplaceAbi.abi, signer)
    console.log('mk', mk)
    // setMarketplace(mk)
    setMarketplace(mk, () => {
      setIsDataLoad(true)
    })
    const nft = new ethers.Contract(NFTAddress.address, NFTAbi.abi, signer)
    setNFT(nft)
    
    

  }
  return (
    <>
    
      <div className='App'>
        
        <>
          <Navigation web3HandlerRemove={web3HandlerRemove} web3Handler={web3Handler} account={account} />
        </>

        <div >
          <div className="home_section">

            {/* {loading ? (
              <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '80vh' }}>
                <Spinner animation="border" style={{ display: 'flex' }} />
                <p className='mx-3 my-0'>Awaiting Metamask Connection...</p>
                
              </div>
              
            ) : ( */}
            {isDataLoad ? <Routes>
              <Route path="/" element={isDataLoad && <Home  setTokID={setTOKID} marketplace={marketplace} nft={nft} buttonLoad={loading} account={account}/>}></Route>
              <Route path="/signup" element={<Signup/>}></Route>
              <Route path="/login" element={<Login account={account} web3Handler={web3Handler} web3HandlerRemove={web3HandlerRemove}/>}></Route>
              <Route path='/create'
                element={loading ? <Spinn /> : <Create marketplace={marketplace} nft={nft} account={account}/>}
              ></Route>
              <Route path='/forgot-password' element={<ForgetPassword/>} />
              <Route path="/my-listed-items" element={loading ? <Spinn /> : <MyListedItems marketplace={marketplace} nft={nft} account={account} />}></Route>
              <Route path="/my-purchases" element={loading ? <Spinn /> : <MyPurchases marketplace={marketplace} nft={nft} account={account} />}></Route>
              <Route path='/detail' element={<Detail forURI={tokID} marketplace={marketplace} nft={nft} />}></Route>
              <Route path='/userprofile/:id'  element={loading ? <Spinn /> : <UserProfile marketplace={marketplace} nft={nft} account={account}/>}
              ></Route>
              <Route path='/loading' element={<Loader/>}></Route>
            </Routes>: "loader" }
              
            
            

            {/* )} */}
          </div>

        </div>
       
        <Footer />
       
      </div>
      </>
  );
}

export default App;
