import React, { useState } from "react";
import imagePlaceholder from "../../Assets/Image PlaceHolder.svg";
import "../App.css";
import "./userprofile.css";
import MyListedItems from "../MyListedItems";
import MyPurchases from "../MyPurchases";
function UserProfile({ marketplace, nft, account }) {
  const [imagePic, setImagePic] = useState();
  return (
    <div className="userProfile_outer">
      <div className="userProfile_back_profile">
        <img
          className="userprofile_back_profile_image"
          src={imagePlaceholder}
        ></img>
      </div>
      <div
        className="userProfile_photo"
        onClick={() => document.querySelector(".image_input_field").click()}
      >
        <input
          type="file"
          className="image_input_field"
          onChange={(e) => setImagePic(URL.createObjectURL(e.target.files[0]))}
          hidden
        />

        {imagePic ? (
          <img className="userProfile_photo_img" src={imagePic}></img>
        ) : (
          <i class="bi bi-file-arrow-up"></i>
        )}
      </div>
      {account ? console.log(account) : <p>''</p>}
      <div class="module_border_wrap">
        <div className="seperation"></div>
      </div>
      <MyPurchases marketplace={marketplace} nft={nft} account={account} />
      <div class="module_border_wrap">
        <div className="seperation"></div>
      </div>
      <MyListedItems marketplace={marketplace} nft={nft} account={account} />
    </div>
  );
}

export default UserProfile;
