import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import { Modal,Button } from "react-bootstrap";
function ChangePswd() {
  const [showModal, setShowModal] = useState(false);
  const { id } = useParams();
  const navigate = useNavigate();
  const [u_name, setU_name] = useState();
  const [password, setPassword] = useState("");
  const [password_message, setPassword_message] = useState("");
  const [temppassword, setTempPassword] = useState("");
  const [temp_password_message, setTemp_Password_message] = useState("");
  const [errMessage, setErrMessage] = useState([]);
  const [message, setMessage] = useState("");
  const [typing, setTyping] = useState(false);
  const [load, setLoad] = useState(false);
  const [nowsubmit, setNowSubmit] = useState(false);
  const handleSubmit = (e) => {
    e.preventDefault();
    if (nowsubmit) {
      axios
        .post(`http://192.168.1.134:3000/auth/forgot-password/${id}`, {
          password: password,
          confirm_password: temppassword,
        })
        .then((response) => {
          console.log(response);
          navigate("/login");
        })
        .catch((error) => {
          console.log("Error", error.message);
        });
      setLoad(true);
    }
  };
  const handleClick = () => {
    setTyping(false);
    setMessage("");
    const passwordRegex =
      /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    if (password === "" || temppassword==="") {
      setShowModal(true);
      return;
    } else if (password.length < 8) {
      setPassword_message("Password must be longer than 7 characters");
    }  else if (password !== temppassword) {
      setTemp_Password_message("Password does not match");
    } else if (!passwordRegex.test(password)) {
      setPassword_message("Must have 1 upper 1 lower 1 special 1 numeric ");
    } else {
      setNowSubmit(true);
    }
  };

  return (
    <>{showModal ? (
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Error</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ color: "red" }}>
          Please fill out all fields.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowModal(false)}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    ) : (
      ""
    )}
      {load ? (
        <div className="change_password_component">
          <p className="form_text">Password has been updated</p>
        </div>
      ) : (
        <div className="change_password_component">
          {typing ? "" : <p className="form_text" style={{color:'red'}}>{errMessage[0]}</p>}
          {typing ? "" : <p className="form_text" style={{color:'red'}}>{message}</p>}
          <form className="form_tag" onSubmit={handleSubmit}>
            <div className="icon_input_container">
              <span className="form_icon_svg_span_forget">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  class="bi bi-file-lock"
                  viewBox="0 0 16 16"
                  style={
                    password_message
                      ? { position: "relative", bottom: "10px", right: "5px" }
                      : {}
                  }
                >
                  <path d="M8 5a1 1 0 0 1 1 1v1H7V6a1 1 0 0 1 1-1zm2 2.076V6a2 2 0 1 0-4 0v1.076c-.54.166-1 .597-1 1.224v2.4c0 .816.781 1.3 1.5 1.3h3c.719 0 1.5-.484 1.5-1.3V8.3c0-.627-.46-1.058-1-1.224zM6.105 8.125A.637.637 0 0 1 6.5 8h3a.64.64 0 0 1 .395.125c.085.068.105.133.105.175v2.4c0 .042-.02.107-.105.175A.637.637 0 0 1 9.5 11h-3a.637.637 0 0 1-.395-.125C6.02 10.807 6 10.742 6 10.7V8.3c0-.042.02-.107.105-.175z" />
                  <path d="M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z" />
                </svg>
              </span>
              <input
                type="text"
                className="form_input_tag"
                placeholder="Enter password"
                onChange={(e) => {
                  setTyping(true);
                  setErrMessage([]);
                  setPassword_message("");
                  setPassword(e.target.value);
                }}
              ></input>
              <br></br>
              {password_message && (
                <div className="validation_message01" style={{ color: "red " }}>
                  {password_message}
                </div>
              )}
            </div>
            <div className="icon_input_container">
              <span className="form_icon_svg_span_forget">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  class="bi bi-file-lock"
                  viewBox="0 0 16 16"
                  style={
                    temp_password_message
                      ? { position: "relative", bottom: "10px", right: "5px" }
                      : {}
                  }
                >
                  <path d="M8 5a1 1 0 0 1 1 1v1H7V6a1 1 0 0 1 1-1zm2 2.076V6a2 2 0 1 0-4 0v1.076c-.54.166-1 .597-1 1.224v2.4c0 .816.781 1.3 1.5 1.3h3c.719 0 1.5-.484 1.5-1.3V8.3c0-.627-.46-1.058-1-1.224zM6.105 8.125A.637.637 0 0 1 6.5 8h3a.64.64 0 0 1 .395.125c.085.068.105.133.105.175v2.4c0 .042-.02.107-.105.175A.637.637 0 0 1 9.5 11h-3a.637.637 0 0 1-.395-.125C6.02 10.807 6 10.742 6 10.7V8.3c0-.042.02-.107.105-.175z" />
                  <path d="M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z" />
                </svg>
              </span>
              <input
                type="text"
                className="form_input_tag"
                placeholder="Retype password"
                onChange={(e) => {
                  setTyping(true);
                  setTemp_Password_message("");
                  setErrMessage([]);
                  setTempPassword(e.target.value);
                }}
              ></input>
              <br></br>
              {temp_password_message && (
                  <div
                    className="validation_message01"
                    style={{ color: "red " }}
                  >
                    {temp_password_message}
                  </div>
                )}
            </div>
            <button
              type="submit"
              className="form_submit_button"
              onClick={() => {
                handleClick();
              }}
            >
              Change Password
            </button>
          </form>
        </div>
      )}
    </>
  );
}

export default ChangePswd;
