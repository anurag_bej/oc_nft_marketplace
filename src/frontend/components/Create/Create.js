import React from "react";
import { useState, useEffect } from "react";
import { ethers } from "ethers";
import { Row, Form, Button, Modal } from "react-bootstrap";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { ImFolderUpload } from "react-icons/im";
import { AiFillFileImage } from "react-icons/ai";
import { MdDelete } from "react-icons/md";
//We are importing ipfsHttpClient this will allow us to upload the metadata about newly created nft to ipfs
import { create } from "ipfs-http-client";
import { Buffer } from "buffer";
import Swal from 'sweetalert2'
const projectId = "2MonXpsWqbTqkm3SQpTOVNbHiek";
const projectSecret = "f0d0dd7d62afa89cceb0981ab28461f3";
const auth =
  "Basic " + Buffer.from(projectId + ":" + projectSecret).toString("base64");
const client = create({
  host: "ipfs.infura.io",
  port: 5001,
  protocol: "https",
  apiPath: "/api/v0",
  headers: {
    authorization: auth,
  },
});

function Create({ marketplace, nft, account }) {
  
  const [errorMessage,setErrorMessage] = useState('')
  const [imagePic, setImagePic] = useState(null);
  const [imageFileName, setImageFileName] = useState("No selected File");
  const [image, setImage] = useState();
  const [imageMessage, setImageMessage] = useState("");
  const [price, setPrice] = useState(null);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [priceMessage, setPriceMessage] = useState("");
  const [nameMessage, setNameMessage] = useState("");
  const [descriptionMessage, setDescriptionMessage] = useState("");
  const [category, setCategory] = useState("Art");
  const [categoryMessage, setCategoryMessage] = useState("");
  const [isSubmitClicked, setIsSubmitClicked] = useState(false);
  const uploadToIPFS = async (event) => {
    event.preventDefault();

    const file = event.target.files[0];
    setImageFileName(event.target.files[0].name);
    setImagePic(URL.createObjectURL(event.target.files[0]));
    if (typeof file !== "undefined") {
      try {
        const result = await client.add(file);
        setImage(`https://oc-nft-marketplace.infura-ipfs.io/ipfs/${result.path}`);
      } catch (error) {
        console.log("Ipfs image upload error ", error);
      }
    }
  };

  useEffect(() => {
    if (isSubmitClicked === true) {
      if (!image) {
        setImageMessage("Image is required");
      } else { 

        setImageMessage("");
      }
    }
  }, [image]);
  useEffect(() => {
    if (!price && isSubmitClicked) {
      setPriceMessage("Price is required");
    } else if (price<0 && isSubmitClicked) {
      setPriceMessage("Price should be more than zero");
    } else {
      setPriceMessage("");
    }
  }, [price]);
  useEffect(() => {
    if (!name && isSubmitClicked) {
      setNameMessage("Name is required");
    } else {
      setNameMessage("");
    }
  }, [name]);
  useEffect(() => {
    if (!description && isSubmitClicked) {
      setDescriptionMessage("Description is required");
    } else {
      setDescriptionMessage("");
    }
  }, [description]);
  useEffect(() => {
    if (!category && isSubmitClicked) {
      setCategoryMessage("Category is required");
    } else {
      setCategoryMessage("");
    }
  }, [category]);
  useEffect(()=>{
    console.log(errorMessage)
  },[errorMessage])
  //It will run when user click submit on the form this will add all metadata of nft to ipfs
  //After it's done interacting with ipfs this function will interact with block chain to mint and then list the nft for sale on the marketplace
  const createNFT = async () => {
    setIsSubmitClicked(true);
    if (!image) {
      setImageMessage("Image is required");
    }
    if (!price) {
      setPriceMessage("Price is required");
    }
    if (price<0) {
      setPriceMessage("Price should be more than zero");
    }
    if (!name) {
      setNameMessage("Name is required");
    }
    if (!description) {
      setDescriptionMessage("Description is required");
    }
    if (!category) {
      setCategoryMessage("Category is required");
    }
    
    if (!image || !price || !name || !description || !category||price<0) {
      return;
    }

    try {
      const result = await client.add(
        JSON.stringify({ image, name, description, account, category })
      );

      
      axios
        .post("http://192.168.1.134:3000/nft/nft-mint", {
          nft_name: name,
          nft_description: description,
          nft_price: parseInt(price),
          nft_image_link: image,
          user: account,
        })
        .then((response) => {
          if(response.data.status==='23505'){
            setErrorMessage('Image already exist')
          }
          if(response.data.status===201){
            mintThenList(result);
            setErrorMessage('')
            Swal.fire(
              'Good job!',
              'You have successfully minted an NFT',
              'success'
            )
          }
          
          
          
        })
        .catch((error) => {
          if (error.response) {
            console.log("Error", error.message);
          } else {
            console.log("Error", error.message);
          }
        });
    
      
    } catch (error) {
      console.log("Ipfs uri upload error ", error);
    }
  };
  const mintThenList = async (result) => {
    const uri = `https://oc-nft-marketplace.infura-ipfs.io/ipfs/${result.path}`;
    //mint the nft
    await (await nft.mint(uri)).wait();
    // get tokenId of new nft
    const id = await nft.tokenCount();
    // approve marketplace to spend nft
    await (await nft.setApprovalForAll(marketplace.address, true)).wait();
    // add nft to marketplace
    const listingPrice = ethers.utils.parseEther(price.toString());
    await (await marketplace.makeItem(nft.address, id, listingPrice)).wait();
    
  };
  function handleChangeCategory(e) {
    setCategory(e.target.value);
  }
  useEffect(() => {
    console.log(isSubmitClicked);
  }, [isSubmitClicked]);
  return (
    <>
      <div className="create_nft_form_outer">
        
        <div className="create_nft_form_inner_div">
          {errorMessage&&(<div className="message_alert" style={{ marginBottom: "10px" }}>
              {errorMessage}
            </div>)}
          {imageMessage && (
            <div className="message_alert" style={{ marginBottom: "10px" }}>
              {imageMessage}
            </div>
          )}
          <div
            className="create_form_image_div"
            onClick={() => document.querySelector(".image_input_field").click()}
          >
            <input
              className="image_input_field"
              type="file"
              required
              name="file"
              onChange={uploadToIPFS}
              hidden
            />
            {imagePic ? (
              <img
                src={imagePic}
                width={400}
                height={200}
                alt={imageFileName}
              ></img>
            ) : (
              <>
                <ImFolderUpload
                  style={{
                    color: "rgba(255, 210, 51, 1)",
                    fontSize: "21vh",
                  }}
                />
                <p className="create_form_image_div_file_upload_text">
                  Drop your files here. PNG, GIF, JPG, WEBP, MP3 Max 100mb.
                  Browse{" "}
                </p>
              </>
            )}
            <section style={{ color: "black" }}>
              <AiFillFileImage color="black" />
              <span tyle={{ padding: "100px" }}>
                {imageFileName}
                <MdDelete
                  onClick={() => {
                    setImageFileName("No selected file ");
                    setImagePic(null);
                    setImage("");
                  }}
                />
              </span>
            </section>
          </div>
          <ul className="create_form_ul">
            <li className="create_form_ul_li">
              {categoryMessage && (
                <div className="message_alert">{categoryMessage}</div>
              )}
              <p className="create_form_ul_li_text">Item Category</p>
              <select
                className="create_form_ul_li_select"
                value={category}
                onChange={handleChangeCategory}
              >
                <option value="Art">Art</option>
                <option value="Gaming">Gaming</option>
              </select>
            </li>
            <br></br>
          </ul>
          <ul className="create_form_ul">
            <li className="create_form_ul_li">
              {nameMessage && (
                <div className="message_alert">{nameMessage}</div>
              )}
              <p className="create_form_ul_li_text">Item Name</p>
              <input
                className="create_form_ul_li_input"
                onChange={(e) => setName(e.target.value)}
                size="lg"
                required
                type="text"
                placeholder="Name"
              ></input>
            </li>
            <li className="create_form_ul_li">
              {priceMessage && (
                <div className="message_alert">{priceMessage}</div>
              )}
              <p className="create_form_ul_li_text">Item Price</p>
              <input
                className="create_form_ul_li_input"
                onChange={(e) => setPrice(e.target.value)}
                required
                type="number"
                placeholder="Price in ETH"
              />
            </li>
          </ul>
          <div className="create_form_description_div">
            {descriptionMessage && (
              <div className="message_alert">{descriptionMessage}</div>
            )}
            <p className="create_form_description_text">Description</p>
            <textarea
              className="create_form_description_input"
              onChange={(e) => setDescription(e.target.value)}
              required
              type="textarea"
              placeholder="Description"
            ></textarea>
          </div>
          <Button
            style={{
              marginTop: "30px",
              backgroundColor: "rgba(162, 89, 255, 1)",
              float: "right",
            }}
            onClick={() => {
              setIsSubmitClicked(true);
              createNFT();
            }}
            size="lg"
            type="submit"
          >
            Create & List NFT!
          </Button>
        </div>
      </div>
    </>
  );
}

export default Create;
// // import React from "react";
// // import { useState } from "react";
// // import { ethers } from "ethers";
// // import { Row, Form, Button, Modal } from "react-bootstrap";
// // import axios from "axios";
// // import { useNavigate } from "react-router-dom";
// // //We are importing ipfsHttpClient this will allow us to upload the metadata about newly created nft to ipfs
// // import { create } from "ipfs-http-client";
// // import { Buffer } from "buffer";
// // const projectId = "2MonXpsWqbTqkm3SQpTOVNbHiek";
// // const projectSecret = "f0d0dd7d62afa89cceb0981ab28461f3";
// // const auth =
// //   "Basic " + Buffer.from(projectId + ":" + projectSecret).toString("base64");
// // const client = create({
// //   host: "ipfs.infura.io",
// //   port: 5001,
// //   protocol: "https",
// //   apiPath: "/api/v0",
// //   headers: {
// //     authorization: auth,
// //   },
// // });

// function Create({ marketplace, nft, account }) {
//   const [showModal, setShowModal] = useState(false);
//   const navigate = useNavigate();
//   const [image, setImage] = useState("");
//   const [price, setPrice] = useState(null);
//   const [name, setName] = useState("");
//   const [description, setDescription] = useState("");

//   const uploadToIPFS = async (event) => {
//     event.preventDefault();
//     const file = event.target.files[0];
//     if (typeof file !== "undefined") {
//       try {
//         const result = await client.add(file);
//         console.log(result);
//         setImage(
//           `https://oc-nft-marketplace.infura-ipfs.io/ipfs/${result.path}`
//         );
//       } catch (error) {
//         console.log("Ipfs image upload error ", error);
//       }
//     }
//   };
//   //It will run when user click submit on the form this will add all metadata of nft to ipfs
//   //After it's done interacting with ipfs this function will interact with block chain to mint and then list the nft for sale on the marketplace
//   const createNFT = async () => {
//     if (!image || !price || !name || !description) {
//       setShowModal(true);
//       return;
//     }
//     try {
//       const result = await client.add(
//         JSON.stringify({ image, name, description })
//       );
//       console.log(result);
//       mintThenList(result);
//       axios
//         .post("http://192.168.1.134:3000/nft/nft-mint", {
//           nft_name: name,
//           nft_description: description,
//           nft_price: price,
//           nft_image_link: image,
//           user: account,
//         })
//         .then((resp) => {
//           console.log(resp.body);
//         })
//         .catch((error) => {
//           if (error.response) {
//             console.log("Error", error.message);
//           } else {
//             console.log("Error", error.message);
//           }
//         });
//     } catch (error) {
//       console.log("Ipfs uri upload error ", error);
//     }
//   };
//   const mintThenList = async (result) => {
//     const uri = `https://oc-nft-marketplace.infura-ipfs.io/ipfs/${result.path}`;
//     //mint the nft
//     await (await nft.mint(uri)).wait();
//     // get tokenId of new nft
//     const id = await nft.tokenCount();
//     // approve marketplace to spend nft
//     await (await nft.setApprovalForAll(marketplace.address, true)).wait();
//     // add nft to marketplace
//     const listingPrice = ethers.utils.parseEther(price.toString());
//     await (await marketplace.makeItem(nft.address, id, listingPrice)).wait();
//   };
//   return (
//     <>
//       {showModal ? (
//         <Modal show={showModal} onHide={() => setShowModal(false)}>
//           <Modal.Header closeButton>
//             <Modal.Title>Error</Modal.Title>
//           </Modal.Header>
//           <Modal.Body style={{ color: "red" }}>
//             Please fill out all required fields.
//           </Modal.Body>
//           <Modal.Footer>
//             <Button variant="secondary" onClick={() => setShowModal(false)}>
//               Close
//             </Button>
//           </Modal.Footer>
//         </Modal>
//       ) : (
//         ""
//       )}
//       <div className="container-fluid mt-5" style={{ height: "60vh" }}>
//         <div className="row">
//           <main
//             role="main"
//             className="col-lg-12 mx-auto"
//             style={{ maxWidth: "1000px" }}
//           >
//             <div className="content mx-auto">
//               <Row className="g-4">
//                 <Form.Control
//                   type="file"
//                   required
//                   name="file"
//                   onChange={uploadToIPFS}
//                 />
//                 {/* <h4 className='create-form-title'>Item Information</h4> */}

//                 <Form.Control
//                   onChange={(e) => setName(e.target.value)}
//                   size="lg"
//                   required
//                   type="text"
//                   placeholder="Name"
//                 />
//                 <Form.Control
//                   onChange={(e) => setDescription(e.target.value)}
//                   size="lg"
//                   required
//                   as="textarea"
//                   placeholder="Description"
//                 />
//                 <Form.Control
//                   onChange={(e) => setPrice(e.target.value)}
//                   size="lg"
//                   required
//                   type="number"
//                   placeholder="Price in ETH"
//                 />
//                 <div className="d-grid px-0"></div>
//               </Row>
//               <Button
//                 style={{
//                   backgroundColor: "rgba(162, 89, 255, 1)",
//                   float: "right",
//                 }}
//                 onClick={createNFT}
//                 size="lg"
//               >
//                 Create & List NFT!
//               </Button>
//             </div>
//           </main>
//         </div>
//       </div>
//     </>
//   );
// }

// export default Create;
