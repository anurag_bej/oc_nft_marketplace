import React,{useEffect, useState} from 'react'
import axios from 'axios'
function Detail({ forTotPrice,forURI, nft, marketplace }) {
  const [img,setImg] = useState([])
  const [loading,setLoading] = useState(true)
  let items = []
  const loadDetails = async () => {
    // const uri = await nft.tokenURI(forURI);  
    console.log(forURI)
    axios.get(forURI)
    .then((response)=>{
      setImg([response.data])
    }
    
    )
    
    // console.log(uri)
    // const response = await fetch(uri)
    // console.log(response)
    // const metadata = await response.json()
    // //const totalPrice = await marketplace.getTotalPrice(forTotPrice)
    // items.push({
    //   name: metadata.name,
    //   description: metadata.description,
    //   image: metadata.image
    // })
    // setLoading(!loading)
  }
  useEffect(()=>{
    loadDetails()
    setLoading(!loading)
  },[])
  if (loading) return (
    <main style={{ padding: "1rem 0" }}>
      <h2>Loading...</h2>
    </main>
  )
  return (
    <h1>
      {img.map((item)=> <>
      <h1>{item.name}</h1>
      <img src={item.image}></img>
      <h3>{item.description}</h3>
      </>)}
    </h1>
  )
}

export default Detail