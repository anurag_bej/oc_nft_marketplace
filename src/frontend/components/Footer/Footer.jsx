import React from 'react'
import './Footer.css'
import {useNavigate,Link} from 'react-router-dom'
function Footer() {
    const navigate = useNavigate()
    return (
        <div className='Footer' style={{ color: 'white' }}>

            <div className='footer-left-div'>
                <h5 className='footer-heading'>NFT Marketplace</h5>
                <p className='footer-text'>NFT marketplace ui created <br />with reactjs</p>
                <h5 className='footer-heading'>Join our community</h5>
                <div className='icon_list'>
                <i class="bi bi-twitter"></i>
                </div>
                <div className='icon_list'>
                <i class="bi bi-facebook"></i>
                </div>
                <div className='icon_list'>
                <i class="bi bi-youtube"></i>
                </div>
                <div className='icon_list'>
                <i class="bi bi-linkedin"></i>
                
                </div>
                <div  style={{display:'block',fontSize:'1vw'}} >copyright @2023 NFT. All right reserved</div>
                

            </div>
            <div className='footer-center-div'>
                <h5 className='footer-heading' >Explore</h5>

                <p className='footer-text' id='footer-home' onClick={()=>{navigate('/')}}>Home</p>

                <p className='footer-text' id='footer-create' onClick={()=>{navigate('/create')}}>Create</p>
                

                <p className='footer-text' id='footer-login' onClick={()=>{navigate('/login')}}>Login</p>
                
                <p className='footer-text' id='footer-signup' onClick={()=>{navigate('/signup')}}>Signup</p>

            </div>


            <div className='footer-right-div'>
                <h5 className='footer-heading'>Join Our Weekly Digest</h5>

                <p className='footer-text'>Get Executive promotions  &<br></br>updates
                straight to your inbox</p>
                <div className='footer_input'>
                    <input className='footer_input_tag' type='email' value='Enter your email here'></input>
                    <button className='footer_button_tag'>Subscribe</button>
                </div>
            </div>



        </div>
    )
}

export default Footer