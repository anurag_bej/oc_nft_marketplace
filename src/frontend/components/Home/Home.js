import { useState, useEffect } from "react";
import { ethers } from "ethers";
import { Row, Col, Card, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import monkey from "../../Assets/monkey.png";
import { ArrowRight } from "react-bootstrap-icons";
import folder from "../../Assets/Vector(2).png";
import folder_hover from "../../Assets/Vector_hover.png";
import "../App.css";
import btc from "../../Assets/bitcoin .png";
import btcash from "../../Assets/bitcoin_cash.png";
import dfinity from "../../Assets/dfinity.png";
import ethereum from "../../Assets/ethereum.png";
import axios from "axios";
import eth_gif from "../../Assets/giphy_eth.gif";
import Swal from 'sweetalert2'
import './Home.css'
const Home = ({ setTokID, marketplace, nft, buttonLoad, account }) => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const [selectCategory,setSelectCategory] = useState('All')
  const [nftCount, setNftCount] = useState();
  const [userCount, setUserCount] = useState();
  const [transactionCount, setTransactionCount] = useState();
  const [sellerNFT, setSellerNFT] = useState("");
  const [buyerNFT, setBuyerNFT] = useState("");
  const [items, setItems] = useState([]);
  const [mp,setMp] = useState(marketplace)
  useEffect(()=>{
    console.log(mp)
  },[mp])
  const loadData = () => {
    axios
      .get("http://192.168.1.134:3000/nft/total-count")
      .then((response) => {
        setNftCount(response.data.nftCount);
        setUserCount(response.data.userCount);
        setTransactionCount(response.data.transactionCount);
      })
      .catch((error) => {
        console.log("Error", error);
      });
  };
  const loadMarketplaceItems = async () => {
    console.log("market place", marketplace)
    const itemCount = await marketplace.itemCount();

    let items = [];
    //console.log('asdsad',items)
    for (let i = 1; i <= itemCount; i++) {
      const item = await marketplace.items(i);
      if (!item.sold) {
        // get uri url from nft contract
        const uri = await nft.tokenURI(item.tokenId);
        setTokID(uri);
        // use uri to fetch the nft metadata stored on ipfs
        const response = await fetch(uri);
        const metadata = await response.json();
        // get total price of item (item price + fee)
        const totalPrice = await marketplace.getTotalPrice(item.itemId);
        // Add item to items array
        items.push({
          totalPrice,
          itemId: item.itemId,
          seller: item.seller,
          name: metadata.name,
          description: metadata.description,
          image: metadata.image,
          category: metadata.category,
        });
      }
    }
    setLoading(!loading);
    setItems(items);

  };
  useEffect(() => {
    console.log(items);
  }, [items]);
  const buyMarketItem = async (item) => {
    if(buttonLoad){
      Swal.fire({
        icon: 'error',
        title: 'Login Required',
        text: 'Metamask wallet is required' 
      })
    }else{
    await (
      await marketplace.purchaseItem(item.itemId, { value: item.totalPrice })
    ).wait();
    axios
      .post("http://192.168.1.134:3000/nft/transaction", {
        sender: account.toLowerCase(),
        receiver: item.seller.toLowerCase(),
        nft: item.image,
      })
      .then((response) => {
        if(response.status===201){
          Swal.fire(
            'NFT Bought!',
            'You have successfully bought an NFT',
            'success'
          )
          setLoading(true)
        }
        console.log(response.data);
      })
      .catch((error) => {
        console.log("Error", error);
      });
    await loadMarketplaceItems();
    loadData();
  }
  };
  useEffect(() => {
    loadMarketplaceItems();
    loadData();
  }, []);
  const [isHovered_1, setIsHovered_1] = useState(false);
  const [isHovered_2, setIsHovered_2] = useState(false);
  const [isHovered_3, setIsHovered_3] = useState(false);

  const handleHover_1 = () => {
    setIsHovered_1(!isHovered_1);
  };
  const handleHover_2 = () => {
    setIsHovered_2(!isHovered_2);
  };
  const handleHover_3 = () => {
    setIsHovered_3(!isHovered_3);
  };
  const notLoaded = () => {
    return (
      <div className="main_page_outer">
        <div className="main_page ">
          <div className="home_left">
            <p className="home_body_text_medium">THE LARGEST NFT MARKETPLACE</p>
            <h1 className="home_body_text_title">
              Discover, Collect & Sell Popular{" "}
              <p className="border_nft">NFTs</p>
            </h1>
            <p className="home_body_text_small">
              The world’s largest marketplace for NFTs <br></br>character
              collections non fungible token NFTs
            </p>
            <button
              className="home_body_button"
              onClick={() => {
                navigate("/login");
              }}
            >
              <i class="bi bi-rocket-takeoff"></i> Connect Wallet
            </button>
            <ul className="ul_list">
              <li className="li_home_list">
                <p className="home_body_number">{nftCount}+</p>
                <p className="li_body_text">Total NFTs</p>
              </li>
              <li className="li_home_list">
                <p className="home_body_number">{userCount}+</p>
                <p className="li_body_text">Total Users</p>
              </li>
              <li className="li_home_list">
                <p className="home_body_number">{transactionCount}+</p>
                <p className="li_body_text">Total Transactions</p>
              </li>
            </ul>
          </div>
          <div className="home_right">
            <img className="home_img" src={monkey}></img>
          </div>
        </div>
        <div class="module_border_wrap">
          <div className="seperation">
            <ul className="seperation_ul">
              <li className="seperation_li">
                <img className="seperation_li_img" src={btc}></img>
                <p className="seperation_li_p">Bitcoin</p>
              </li>
              <li className="seperation_li">
                <img className="seperation_li_img" src={ethereum}></img>
                <p className="seperation_li_p">Ethereum</p>
              </li>
              <li className="seperation_li">
                <img className="seperation_li_img" src={dfinity}></img>
                <p className="seperation_li_p">Dfinity</p>
              </li>
              <li className="seperation_li">
                <img className="seperation_li_img_last" src={btcash}></img>
                <p className="seperation_li_p_last">Bitcoin Cash</p>
              </li>
            </ul>
          </div>
        </div>
        <div className="nft_item_box">
          <div className="nft_item_heading">
            <h1 className="nft_item_h1">All Collection</h1>
            <p className="nft_item_p">
              Checkout our all collection updated Trading Collection
            </p>
            <div className="flex justify-center">
              {items.length > 0 ? (
                <div className="px-5 container" style={{ color: "black" }}>
                  <Row xs={1} md={2} lg={3} className="g-2 py-5">
                    {items.map((item, idx) => (
                      (item.category===selectCategory || selectCategory==='All' )?(
                        <Col key={idx} className="overflow-hidden">
                        <Card
                          style={{
                            background: "rgba(255, 255, 255, 0.1)",
                            height: "400px",
                          }}
                          onClick={() => {
                            console.log("item", item);
                          }}
                        >
                          <Card.Img
                            variant="top"
                            style={{ padding: "20px", paddingBottom: "0px" }}
                            src={item.image}
                          />
                          <Card.Body
                            color="secondary"
                            style={{ 
                              color: "white" ,
                              height:'100px'}}
                          >
                            <Card.Text
                              style={{
                                float: "left",
                                color: "rgba(255, 255, 255, 0.5)",
                                padding: "10px",
                              }}
                            >
                              @{item.category}
                            </Card.Text>
                            <br></br>
                            <Card.Title
                              style={{
                                position: "relative",
                                right: "48%",
                                color: "white",
                                fontWeight: "bolder",
                                fontSize: "20px",
                                padding: "20px",
                                paddingLeft: "0",
                              }}
                            >
                              {item.name}
                            </Card.Title>

                            <Card.Text
                              style={{
                                position: "relative",
                                left: "110px",
                                bottom: "80px",
                                marginBottom: "0",
                              }}
                            >
                              <p
                                style={{
                                  color: "rgba(255, 255, 255, 0.5)",
                                  display: "inline"
                                }}
                              >
                                Price
                              </p>
                              <br></br>
                              {ethers.utils.formatEther(item.totalPrice)}{" "} ETH
                            </Card.Text>
                          </Card.Body>
                          <Card.Footer>
                            <div className="d-grid">
                              
                                <Button
                                  onClick={() => buyMarketItem(item)}
                                  variant="primary"
                                  size="lg"
                                  style={{
                                    background:
                                      " linear-gradient(214.02deg, #B75CFF 6.04%, #671AE4 92.95%)",
                                    outline: "none",
                                    border: "none",
                                  }}
                                >
                                  Buy
                                </Button>
                            </div>
                          </Card.Footer>
                        </Card>
                      </Col>
                      ):''
                      
                    ))}
                  </Row>
                </div>
              ) : (
                <main style={{ padding: "1rem 0", color: "white" }}>
                  <h2>No listed assets</h2>
                </main>
              )}
            </div>
          </div>
        </div>

        <div class="module_border_wrap">
          <div className="seperation" style={{ height: "150px" }}>
            <div className="what_is_eth_">
              <div className="what_is_eth_gif">
                <img src={eth_gif}></img>
              </div>
              <div className="what_is_eth_text">
                <p className="what_is_eth_text_header">What is Ethereum?</p>
                <p className="what_is_eth_text_body">
                  Ethereum is a decentralized blockchain platform that
                  establishes a peer-to-peer network that securely executes and
                  verify smart contracts. The ethereum platform builds of block-
                  chain technology.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="nft_item_box">
          <div className="nft_item_heading">
            <h1 className="nft_item_h1">How it works</h1>
            <p className="nft_item_p">Find out how to get started</p>
            <div className="how_it_work_card_list">
              <div
                className="outer_how_it_work_card_item"
                onMouseEnter={handleHover_1}
                onMouseLeave={handleHover_1}
              >
                <div className="how_it_work_card_item">
                  <img
                    src={isHovered_1 ? folder_hover : folder}
                    className="how_it_work_img"
                  ></img>
                  <div className="how_it_work_card_step_heading">Step 1</div>
                  <div className="how_it_work_card_heading">
                    Set up your wallet
                  </div>
                  <div className="how_it_work_card_text">
                    Set up your wallet of choice Connect it to the OC
                    marketplace by clicking the wallet icon in the left corner.
                  </div>
                </div>
              </div>
              <div
                className="outer_how_it_work_card_item"
                onMouseEnter={handleHover_2}
                onMouseLeave={handleHover_2}
              >
                <div className="how_it_work_card_item">
                  <img
                    src={isHovered_2 ? folder_hover : folder}
                    className="how_it_work_img"
                  ></img>
                  <div className="how_it_work_card_step_heading">Step 2</div>
                  <div className="how_it_work_card_heading">
                    Upload & Create Collection
                  </div>
                  <div className="how_it_work_card_text">
                    Upload your work and setup your collection. Add a
                    description, social link and price.
                  </div>
                </div>
              </div>
              <div
                className="outer_how_it_work_card_item"
                onMouseEnter={handleHover_3}
                onMouseLeave={handleHover_3}
              >
                <div className="how_it_work_card_item">
                  <img
                    src={isHovered_3 ? folder_hover : folder}
                    className="how_it_work_img"
                  ></img>
                  <div className="how_it_work_card_step_heading">Step 3</div>
                  <div className="how_it_work_card_heading">Start Earning</div>
                  <div className="how_it_work_card_text">
                    Choose between auction and fixed price listing. Start
                    earning by selling your NFTs or trading others.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
  const Loaded = () => {
    return (
      <>
        <div className="main_page ">
          <div className="home_left">
            <p className="home_body_text_medium">THE LARGEST NFT MARKETPLACE</p>
            <h1 className="home_body_text_title">
              Discover, Collect & Sell Popular{" "}
              <p className="border_nft">NFTs</p>
            </h1>
            <p className="home_body_text_small">
              The world’s largest marketplace for NFTs <br></br>character
              collections non fungible token NFTs
            </p>
            <ul className="ul_list">
              <li className="li_home_list">
                <p className="home_body_number">{nftCount}+</p>
                <p className="li_body_text">Total NFTs</p>
              </li>
              <li className="li_home_list">
                <p className="home_body_number">{userCount}+</p>
                <p className="li_body_text">Total Users</p>
              </li>
              <li className="li_home_list">
                <p className="home_body_number">{transactionCount}+</p>
                <p className="li_body_text">Total Transactions</p>
              </li>
            </ul>
          </div>
          <div className="home_right">
            <img className="home_img" src={monkey}></img>
          </div>
        </div>
        <div class="module_border_wrap">
          <div className="seperation">
            <ul className="seperation_ul">
              <li className="seperation_li">
                <img className="seperation_li_img" src={btc}></img>
                <p className="seperation_li_p">Bitcoin</p>
              </li>
              <li className="seperation_li">
                <img className="seperation_li_img" src={ethereum}></img>
                <p className="seperation_li_p">Ethereum</p>
              </li>
              <li className="seperation_li">
                <img className="seperation_li_img" src={dfinity}></img>
                <p className="seperation_li_p">Dfinity</p>
              </li>
              <li className="seperation_li">
                <img className="seperation_li_img_last" src={btcash}></img>
                <p className="seperation_li_p_last">Bitcoin Cash</p>
              </li>
            </ul>
          </div>
        </div>
        <div className="nft_item_box">
          <div className="nft_item_heading">
            <h1 className="nft_item_h1">All Collection</h1>
            <p className="nft_item_p">
              Checkout our all collection updated Trading Collection
            </p>
            <div className='nft_item_select_category'>
              <ul className='nft_item_select_category_ul'>
                <li className='nft_item_select_category_li'><button  id='All' className='nft_item_select_category_button'  onClick={()=>setSelectCategory('All')}>All</button></li>
                <li className='nft_item_select_category_li'><button  id='Art' className='nft_item_select_category_button'  onClick={()=>setSelectCategory('Art')}>Art</button></li>
                <li className='nft_item_select_category_li'><button  id='Gaming' className='nft_item_select_category_button'  onClick={()=>setSelectCategory('Gaming')}>Gaming</button></li>
              </ul>
            </div>
            <div className="flex justify-center">
              {items.length > 0 ? (
                <div className="px-5 container" style={{ color: "black" }}>
                  <Row xs={1} md={2} lg={3} className="g-2 py-5">
                    {items.map((item, idx) => (
                      (item.category===selectCategory || selectCategory==='All' )?(
                        <Col key={idx} className="overflow-hidden">
                        <Card
                          style={{
                            background: "rgba(255, 255, 255, 0.1)",
                            height: "400px",
                          }}
                          onClick={() => {
                            console.log("item", item);
                          }}
                        >
                          <Card.Img
                            variant="top"
                            style={{ padding: "20px", paddingBottom: "0px" }}
                            src={item.image}
                          />
                          <Card.Body
                            color="secondary"
                            style={{ 
                              color: "white" ,
                              height:'100px'}}
                          >
                            <Card.Text
                              style={{
                                float: "left",
                                color: "rgba(255, 255, 255, 0.5)",
                                padding: "10px",
                              }}
                            >
                              @{item.category}
                            </Card.Text>
                            <br></br>
                            <Card.Title
                              style={{
                                position: "relative",
                                right: "48%",
                                color: "white",
                                fontWeight: "bolder",
                                fontSize: "20px",
                                padding: "20px",
                                paddingLeft: "0",
                              }}
                            >
                              {item.name}
                            </Card.Title>

                            <Card.Text
                              style={{
                                position: "relative",
                                left: "110px",
                                bottom: "80px",
                                marginBottom: "0",
                              }}
                            >
                              <p
                                style={{
                                  color: "rgba(255, 255, 255, 0.5)",
                                  display: "inline"
                                }}
                              >
                                Price
                              </p>
                              <br></br>
                              {ethers.utils.formatEther(item.totalPrice)}{" "} ETH
                            </Card.Text>
                          </Card.Body>
                          <Card.Footer>
                            <div className="d-grid">
                              
                                <Button
                                  onClick={() => buyMarketItem(item)}
                                  variant="primary"
                                  size="lg"
                                  style={{
                                    background:
                                      " linear-gradient(214.02deg, #B75CFF 6.04%, #671AE4 92.95%)",
                                    outline: "none",
                                    border: "none",
                                  }}
                                >
                                  Buy
                                </Button>
                            </div>
                          </Card.Footer>
                        </Card>
                      </Col>
                      ):''
                      
                    ))}
                  </Row>
                </div>
              ) : (
                <main style={{ padding: "1rem 0", color: "white" }}>
                  <h2>No listed assets</h2>
                </main>
              )}
            </div>
          </div>
        </div>

        <div class="module_border_wrap">
          <div className="seperation" style={{ height: "150px" }}>
            <div className="what_is_eth_">
              <div className="what_is_eth_gif">
                <img src={eth_gif}></img>
              </div>
              <div className="what_is_eth_text">
                <p className="what_is_eth_text_header">What is Ethereum?</p>
                <p className="what_is_eth_text_body">
                  Ethereum is a decentralized blockchain platform that
                  establishes a peer-to-peer network that securely executes and
                  verify smart contracts. The ethereum platform builds of block-
                  chain technology.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="nft_item_box">
          <div className="nft_item_heading">
            <h1 className="nft_item_h1">How it works</h1>
            <p className="nft_item_p">Find out how to get started</p>
            <div className="how_it_work_card_list">
              <div
                className="outer_how_it_work_card_item"
                onMouseEnter={handleHover_1}
                onMouseLeave={handleHover_1}
              >
                <div className="how_it_work_card_item">
                  <img
                    src={isHovered_1 ? folder_hover : folder}
                    className="how_it_work_img"
                  ></img>
                  <div className="how_it_work_card_step_heading">Step 1</div>
                  <div className="how_it_work_card_heading">
                    Set up your wallet
                  </div>
                  <div className="how_it_work_card_text">
                    Set up your wallet of choice Connect it to the OC
                    marketplace by clicking the wallet icon in the left corner.
                  </div>
                </div>
              </div>
              <div
                className="outer_how_it_work_card_item"
                onMouseEnter={handleHover_2}
                onMouseLeave={handleHover_2}
              >
                <div className="how_it_work_card_item">
                  <img
                    src={isHovered_2 ? folder_hover : folder}
                    className="how_it_work_img"
                  ></img>
                  <div className="how_it_work_card_step_heading">Step 2</div>
                  <div className="how_it_work_card_heading">
                    Upload & Create Collection
                  </div>
                  <div className="how_it_work_card_text">
                    Upload your work and setup your collection. Add a
                    description, social link and price.
                  </div>
                </div>
              </div>
              <div
                className="outer_how_it_work_card_item"
                onMouseEnter={handleHover_3}
                onMouseLeave={handleHover_3}
              >
                <div className="how_it_work_card_item">
                  <img
                    src={isHovered_3 ? folder_hover : folder}
                    className="how_it_work_img"
                  ></img>
                  <div className="how_it_work_card_step_heading">Step 3</div>
                  <div className="how_it_work_card_heading">Start Earning</div>
                  <div className="how_it_work_card_text">
                    Choose between auction and fixed price listing. Start
                    earning by selling your NFTs or trading others.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };
  return buttonLoad ? notLoaded() : Loaded();
};

export default Home;
